<?php
/**
 * @file
 * API Documentation.
 */

/**
 * Implements hook_form_file_element_alter().
 */
function hook_form_file_element_alter(&$elements) {
  $elements = array(
    // Form ID.
    'page_node_form' => array(
      // Field machine name.
      'field_file_convert' => array(
        // Watermark text.
        'watermark_text' => t('My cool watermark!'),
        // Field language.
        'language' => LANGUAGE_NONE,
      ),
    ),
  );
}
