<?php
/**
 * @file
 * Extend the FPDF classes to rotate text.
 */

/**
 * Class PDF
 * Needed in order to place text along spine of PDF documents.
 */
class FileConvertPdf extends FileConvertPdfRotate {

  /**
   * Rotate text around its origin.
   */
  public function rotatedText($x, $y, $txt, $angle) {
    // Text rotated around its origin.
    $this->Rotate($angle, $x, $y);
    $this->SetAlpha(0.5);
    $this->Text($x, $y, $txt);
    $this->Rotate(0);
  }

  /**
   * Rotate image around its upper-left corner.
   */
  public function rotatedImage($file, $x, $y, $w, $h, $angle) {
    // Image rotated around its upper-left corner.
    $this->Rotate($angle, $x, $y);
    $this->Image($file, $x, $y, $w, $h);
    $this->Rotate(0);
  }
}
