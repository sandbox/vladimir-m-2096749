<?php
/**
 * @file
 * Extend FPDF Class.
 */

/**
 * Class pdf_rotate.
 */
class FileConvertPdfRotate extends FPDI {
  var $angle = 0;
  var $extgstates;

  /**
   * Function setAlpha().
   */
  function setAlpha($alpha, $bm = 'Normal') {
    // set alpha for stroking (CA) and non-stroking (ca) operations
    $gs = $this->AddExtGState(array(
      'ca' => $alpha,
      'CA' => $alpha,
      'BM' => '/' . $bm
    ));
    $this->setExtGState($gs);
  }

  /**
   * Function setExtGState().
   */
  function setExtGState($gs) {
    $this->_out(sprintf('/GS%d gs', $gs));
  }

  /**
   * Function addExtGState().
   */
  function addExtGState($parms) {
    $n = count($this->extgstates) + 1;
    $this->extgstates[$n]['parms'] = $parms;
    return $n;
  }

  /**
   * Function Rotate().
   */
  public function rotate($angle, $x = -1, $y = -1) {
    if ($x == -1) {
      $x = $this->x;
    }
    if ($y == -1) {
      $y = $this->y;
    }
    if ($this->angle != 0) {
      $this->_out('Q');
    }
    $this->angle = $angle;
    if ($angle != 0) {
      $angle *= M_PI / 180;
      $c = cos($angle);
      $s = sin($angle);
      $cx = $x * $this->k;
      $cy = ($this->h - $y) * $this->k;
      $this->_out(sprintf('q %.5F %.5F %.5F %.5F %.2F %.2F cm 1 0 0 1 %.2F %.2F cm', $c, $s, -$s, $c, $cx, $cy, -$cx, -$cy));
    }
  }

  /**
   * Function _endpage().
   */
  public function _endpage() {
    if ($this->angle != 0) {
      $this->angle = 0;
      $this->_out('Q');
    }
    parent::_endpage();
  }

  /**
   * Function _putextgstates().
   */
  function _putextgstates() {
    for ($i = 1; $i <= count($this->extgstates); $i++) {
      $this->_newobj();
      $this->extgstates[$i]['n'] = $this->n;
      $this->_out('<</Type /ExtGState');
      foreach ($this->extgstates[$i]['parms'] as $k => $v) {
        $this->_out('/' . $k . ' ' . $v);
      }
      $this->_out('>>');
      $this->_out('endobj');
    }
  }

  /**
   * Function _putresourcedict().
   */
  function _putresourcedict() {
    parent::_putresourcedict();
    $this->_out('/ExtGState <<');
    foreach ($this->extgstates as $k => $extgstate) {
      $this->_out('/GS' . $k . ' ' . $extgstate['n'] . ' 0 R');
    }
    $this->_out('>>');
  }

  /**
   * Function _putresources().
   */
  function _putresources() {
    $this->_putextgstates();
    parent::_putresources();
  }
}
